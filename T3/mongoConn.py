# -*- coding: utf-8 -*-
from pymongo import MongoClient

def get_db():
    client = MongoClient('clusterbigdata57.virtual.uniandes.edu.co:27017')
    db = client.Grupo08
    return db
    
def get_Tweet(db):
    return db.Tweets.find_one()