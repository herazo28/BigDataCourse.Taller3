# -*- coding: utf-8 -*-
import MongoConn
from bson.objectid import ObjectId

class Tree(object):
    def __init__(self):
        self.left = None
        self.right = None
        self.data = None
        self.reduced = False
        
    def __str__(self):
        representation = "data: " + str(self.data) + "\n reduced: " + str(self.reduced)
        if self.left != None:
            representation += "\n\tNodo izquierdo: " + str(self.left)            
        if self.right != None:
            representation += "\n\tNodo derecho:" + str(self.right)        
        return  representation
        
    
    
def preOrder(nodo, lista):
    lista.append(nodo.data) 
    if nodo.left != None:
        preOrder(nodo.left, lista)
    if nodo.right != None:
        preOrder(nodo.right, lista)
    return lista
    
    

def convertToMongoQuery(queryTerms):
    
    if '"' in queryTerms:
        queryTerms = queryTerms.replace('"', chr(92) + '"')
    
    if 'NOT ' in queryTerms:
        queryTerms = queryTerms.replace('NOT ', '-')
        
    if ' OR ' in queryTerms:
        queryTerms = queryTerms.replace(' OR ', ' ')
        
    return queryTerms.strip()
        
    
def query(queryTerms):
    
    db = MongoConn.get_db()
    if ' AND ' not in queryTerms:
        queryTerms = convertToMongoQuery(queryTerms)
        results = db.Tweets.find({'$text': {'$search': queryTerms } }, {"id_str": 1, "user.screen_name": 1, "created_at": 1, "text": 1, "entities.hashtags": 1 })
    else:
        querySplit = queryTerms.split(' AND ')
        searchSpace = None
        for query in querySplit:
            if searchSpace == None:
                busqueda = db.Tweets.find({"$text": {"$search": query } }, { "_id": 1})    
                searchSpace = []
                for i in busqueda:
                    searchSpace.append(ObjectId(i["_id"]))
            else:
                busqueda = db.Tweets.find({"$text": {"$search": query }, "_id": { "$in": searchSpace} }, { "_id": 1})    
                searchSpace = []
                for i in busqueda:
                    searchSpace.append(ObjectId(i["_id"]))
                    
        results = db.Tweets.find({"_id": { "$in": searchSpace} }, {"id_str": 1, "user.screen_name": 1, "created_at": 1, "text": 1, "entities.hashtags": 1})         
    
    resultadoHashTags = ""
    resultadoTweets = []
    for document in results:
        tmpDict = {}
        tmpDict['user'] = document['user']['screen_name']
        tmpDict['time'] = document['created_at']
        tmpDict['texto'] = document['text']
        #classification = getClassification(document['id_str'])
        #tmpDict['polaridad'] =  classification[0]      
        #tmpDict['sentimiento'] =  classification[1]
        resultadoTweets.append(tmpDict)
        
        for i in document['entities']['hashtags']:
            resultadoHashTags += " " + i['text']
        
    resultado = (resultadoTweets, resultadoHashTags)
    return resultado
    
    
def query2(queryTerms):
    queryTerms = queryTerms.strip()
    
    arbol = Tree()
    arbol.data = queryTerms  
    
    expandTree(arbol)
    
    print(arbol)
    #lista = []
    #lista = preOrder(arbol, lista)
    
    #print(lista)    
    
    #while(not arbol.reduced):
    #    reduceNode(arbol)
        
        
    
     
     
            
        
        
    print(arbol.data)
        


def reduceNode(node):
    if node.left.reduced and node.right.reduced:
        if node.data == '&&':
            node.data = node.left.data & node.right.data
            node.left = None
            node.right = None
            node.reduced = True
        elif node.data == '||':
            node.data = node.left.data | node.right.data
            node.left = None
            node.right = None
            node.reduced = True
    else:
        if not node.left.reduced:
            reduceNode(node.left)
        if not node.right.reduced:
            reduceNode(node.right)
    

            
        
def expandTree(root):
    expandir = expandNode(root)
    print('expandir: ' + str(expandir))
    if expandir:
        expandNode(root.left)
        expandNode(root.right)        
    
def expandNode(node):
    db = MongoConn.get_db()
    print()
    print(node.data)
    texto = dividirTexto(node.data)
    print(texto)
    cambio = False
    if len(texto) == 3:
        node.data = texto[1]
        left = Tree()
        left.data = texto[0]        
        node.left = left
        right = Tree()
        right.data = texto[2]
        node.right = right
        cambio = True
    else:
        node.data = texto[0]
        node.reduced = True
        """
        busqueda = db.Tweets.find({"$text": {"$search": texto[0] } }, { "_id": 1})    
        searchSpace = []
        for i in busqueda:
            searchSpace.append(ObjectId(i["_id"]))
        node.data = searchSpace
        
        """
        
    return cambio
    
def dividirTexto(texto):
    indexAND = texto.find('&&')
    indexOR  = texto.find('||')
    indexPAR = texto.find('(')
    
    if '&&' == texto or '||' == texto:
        return [texto]
    
    if indexAND < 0:
        indexAND = indexAND * -1000
    if indexOR < 0:
        indexOR = indexOR * -1000
    if indexPAR < 0:
        indexPAR = indexPAR * -1000
    
    resultado = []
    if indexAND < indexOR and indexAND < indexPAR and indexAND >= 0:
        resultado.append(texto[0:indexAND].strip())
        resultado.append('&&')
        resultado.append(texto[indexAND+2:].strip())
    elif indexOR < indexAND and indexOR < indexPAR and indexOR >= 0:    
        resultado.append(texto[0:indexOR].strip())
        resultado.append('||')
        resultado.append(texto[indexOR+2:].strip())
    elif indexPAR < indexAND and indexPAR < indexOR and indexPAR >= 0:    
        valor = 0
        for i in range(0, len(texto)):
            if texto[i] == '(':
                valor += 1
            elif texto[i] == ')':
                valor -= 1
            
            if valor == 0 and i == len(texto) - 1:
                resultado = dividirTexto(texto[1:-1])
            elif valor == 0:
                resultado.append(texto[0:i+1].strip())
                resultado.append(texto[i+1:i+5].strip())
                resultado.append(texto[i+4:].strip())
                break
    else:
        return [texto]
    
    return resultado
            
        #for i in range(0, len(queryTerms)):
        #    print(queryTerms[i])
query2('(Mocoa || DIAN) || fraude')    
    
    
#a = dividirTexto('(2 || 3) || 1')
#print(a)
#a = dividirTexto('(2 || 3) || (3 || 4)')
#print(a)
#a = dividirTexto('(3 || 4)')
#print(a)
#a = dividirTexto('3')
#print(a)
#a = dividirTexto('&&')
#print(a)

#query2('1 && (2 || 3)')
#print('---- Segunda expresion----')
#query2('(1 && (2 || 3)) && (4 || 5)')
#print('---- Tercera expresion----')
#query2('1 && (2 || 3) && (4 || 5)')
    
    
    