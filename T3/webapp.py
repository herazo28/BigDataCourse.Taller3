# -*- coding: utf-8 -*-

from flask import Flask, request
from flask import render_template
import mongoQuery


import mongoConn
import word_cloud



app = Flask(__name__)

@app.route('/')
def index():
    return render_template('general/grupoinfo.html')

@app.route('/grupoinfo')
def grupoinfo():
    return render_template('general/grupoinfo.html')
    
@app.route('/modelo')
def modelo():
    return render_template('general/modelo.html')
    
@app.route('/busqueda', methods=['GET', 'POST'])
def busqueda():
    if request.method == 'POST':
        searchTerms = request.form.get('searchTerms')        
        registros = mongoQuery.query(searchTerms)
        word_cloud_img = None
        if len(registros[1]) > 0:
            word_cloud_img = word_cloud.generateCloud(registros[1])
        return render_template('general/busqueda.html', registros=registros[0], img=word_cloud_img, searchTerms=searchTerms)
    else:
        return render_template('general/busqueda.html')
    

@app.route('/indice', methods=['GET', 'POST'])
def indice():
    if request.method == 'POST':
        user = request.form.get('user')   
        resultado = mongoQuery.getInfo(user)
        registros = mongoQuery.getUsers()
        return render_template('general/indice.html', users=registros, polaridad=resultado[0], sentimiento=resultado[1])
    else:
        registros = mongoQuery.getUsers()
        return render_template('general/indice.html', users=registros)



@app.route('/testMongo')
def test():
    #render_template('test.html')
    db = mongoConn.get_db()
    tweet = mongoConn.get_Tweet(db)
    #print(tweet)
    return str(tweet)

app.run(debug=True, host ='0.0.0.0', port=8082)
