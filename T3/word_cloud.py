# -*- coding: utf-8 -*-
from wordcloud import WordCloud
import time
from os import path

def generateCloud(pText):
    # Generate a word cloud image
    stopwords = []
    wordcloud = WordCloud(background_color="white",stopwords=stopwords).generate(pText)
    millis = int(round(time.time() * 1000))
    
    d = path.dirname(__file__) + '/static/general/word_cloud'
    nombre = str(millis) + ".png"
    
    wordcloud.to_file(path.join(d, nombre))
    
    return nombre